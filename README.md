# OpenML dataset: colleges_aaup

https://www.openml.org/d/488

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

The AAUP dataset for the ASA Statistical Graphics Section's 1995
Data Analysis Exposition contains information on faculty salaries
for 1161 American colleges and universities.  The data may be
obtained in either of two formats.

AAUP.DATA contains the raw data in comma delimited fields with a
single data line for each school. The order of variables is the
same as given below for the fixed column version, although the
spacing varies for each school.

AAUP2.DATA has the data arranged in fixed columns, with two data
lines for each school and a maximum line length of 80 characters.

This dataset is taken from the March-April 1994 issue of Academe.
Thanks to Maryse Eymonerie, Consultant to AAUP, for assistance in
supplying the data.  Faculty salary data are for the 1993-94
school year. You may wish to consult a copy of the special issue
of Academe for more detailed descriptions of the variables.

Data Revised: Wed Jan 18 1995

VARIABLE DESCRIPTIONS (AAUP2.DAT)
Fixed column format with two data lines per school

Line #1
1 -  5   FICE (Federal ID number)
7 - 37   College name
38 - 39   State (postal code)
40 - 43   Type  (I, IIA, or IIB)
44 - 48   Average salary - full professors
49 - 52   Average salary - associate professors
53 - 56   Average salary - assistant professors
57 - 60   Average salary - all ranks
61 - 65   Average compensation - full professors
66 - 69   Average compensation - associate professors
70 - 73   Average compensation - assistant professors
74 - 78   Average compensation - all ranks

Line #2
1 -  4   Number of full professors
5 -  8   Number of associate professors
9 - 12   Number of assistant professors
13 - 16   Number of instructors
17 - 21   Number of faculty - all ranks

Missing values are denoted with *
All salary and compensation figures are yearly in $100's

**************************************************************
To obtain the dataset from Statlib, send one of the single line
messages below to the address statlib@lib.stat.cmu.edu

send aaup.data from colleges
or
send aaup2.data from colleges


For more information on the ASA Statistical Graphics Section's
1995 Data Analysis Exposition send the message

send readme from colleges

%%%%%%%%%%%%%%
INFORMATION %
%%%%%%%%%%%%%%

WHAT'S WHAT AMONG AMERICAN COLLEGES AND UNIVERSITIES?

This is the subject of the 1995 Data Analysis Exposition
sponsored by the Statistical Graphics Section of the American
Statistical Association.  The purpose of the Exposition is to
encourage statisticians to demonstrate techniques, especially
graphical, for analyzing data and displaying the results of an
analysis.  Individuals and groups will work with the same set of
data and present their analyses at a special session as part of
the annual Joint Statistical Meetings in Orlando, Florida on
August 13th-17th, 1995.  The datasets for 1995 are drawn from two
sources, U.S. News & World Report's Guide to Americas Best
Colleges and the AAUP (American Association of University
Professors) 1994 Salary Survey which appeared in the March-April
1994 issue of Academe.

The U.S. News data contains information on tuition, room & board
costs, SAT or ACT scores, application/acceptance rates,
graduation rate, student/faculty ratio, spending per student, and
a number of other variables for 1300+ schools. The AAUP data
includes average salary, overall compensation, and number of
faculty broken down by full, associate, and assistant professor
ranks.

The raw data and documentation are contained in the files
described below.  To obtain any of these files send a message to
statlib@lib.stat.cmu.edu of the following form  (substituting the
file you want for XXXXX)

send XXXXX from colleges

Available files

usnews.doc      Documentation for the U.S. News data
usnews.data     U.S. News data in comma delimited format
usnews3.data    U.S. News data in fixed column format

aaup.doc        Documentation for the AAUP salary data
aaup.data       AAUP salary data in comma delimited format
aaup2.data      AAUP salary data in fixed column format

Two versions of each dataset are provided to accommodate users
with different software constraints.  The comma delimited
versions (USNEWS.DATA and AAUP.DATA) contain information for each
college on a separate line with values delimited by commas.  The
fixed column versions (USNEWS3.DATA and AAUP2.DATA) use 2 or 3
data lines per school and a maximum line length of 80 characters.

To participate in the 1995 Data Analysis Exposition you must send
an abstract form to the American Statistical Association by
February 1st, 1995.  Information is available from the ASA
Meetings Department by e-mail (meetings@asa.mhs.compuserve.com),
phone (703-684-1221), fax (703-684-2037), or surface mail (ASA,
1429 Duke St., Alexandria, VA 22314).  Your initial abstract may
be fairly general since you may do the bulk of your analysis
after the February 1 deadline.

You may choose your own path to proceed in analyzing the data or
use some of the suggested questions below to get started.

... How well can we model tuition using the other variables?
... How might we cluster colleges into similar comparison groups?
... How can we best display faculty salary structure?
... Can we find a reasonable way to rank the schools?

You may work on your own or put together a team.  Show off the
capabilities of your favorite software package or use the data
for a class project and display your students results.  You may
choose to consider just a subset of schools or examine regional
patterns.  The main point is to find innovative ways to display
the interesting features of the data.

Further questions about the 1995 Exposition can be directed to
Robin Lock, Mathematics Department, St. Lawrence University,
Canton, NY 13617  e-mail   rlock@vm.stlawu.edu

If you would like to be informed about any subsequent adjustments
or error fixes to the 1995 Exposition data, please send an e-mail
message to register your interest to rlock@vm.stlawu.edu.

Special thanks for providing data for the 1995 Exposition to:
Robert Morse, Director of Research for America's Best Colleges at
U.S. News & World Report
Maryse Eymonerie, Consultant to AAUP.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/488) of an [OpenML dataset](https://www.openml.org/d/488). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/488/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/488/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/488/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

